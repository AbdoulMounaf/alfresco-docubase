<import resource="classpath:/alfresco/templates/org/alfresco/import/alfresco-util.js">
var regionId = args['region-id'];
function main()
{
   var myDocs = {
      id : "MesDerniersDocuments",
      name : "Atos.docubase.dashlet.MesDerniersDocuments",
      options : {         
         regionId : regionId
      }
   };
   model.widgets = [myDocs];
}
main();