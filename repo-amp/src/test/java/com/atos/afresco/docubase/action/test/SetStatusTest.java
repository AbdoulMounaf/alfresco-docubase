package com.atos.afresco.docubase.action.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.nodelocator.NodeLocatorService;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.QName;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atos.alfresco.docubase.action.SetStatusAction;
import com.atos.alfresco.docubase.model.DocubaseModel;
import com.tradeshift.test.remote.Remote;
import com.tradeshift.test.remote.RemoteTestRunner;

@RunWith(RemoteTestRunner.class)
@Remote(runnerClass = SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:alfresco/application-context.xml")
public class SetStatusTest {

	@Autowired
	@Qualifier("ActionService")
	protected ActionService actionService;

	@Autowired
	@Qualifier("FileFolderService")
	protected FileFolderService fileFolderService;

	@Autowired
	@Qualifier("NodeService")
	protected NodeService nodeService;

	@Autowired
	@Qualifier("nodeLocatorService")
	protected NodeLocatorService nodeLocatorService;

	@Autowired
	@Qualifier("AuthorityService")
	protected AuthorityService authorityService;
	
	@Autowired
	@Qualifier("PersonService")
	protected PersonService personService;

	@Autowired
	@Qualifier("PermissionService")
	protected PermissionService permissionService;
	
	protected String groupName = "TestGroup";
	protected String userOfGroup = "user1";
	protected String userOutOfGroup = "user2";

	@Before
	public void setUp() {
		AuthenticationUtil.setFullyAuthenticatedUser(AuthenticationUtil.getAdminUserName());
		// Création du groupe de test. Attention, ne pas mettre le préfixe
		// GROUP_
		this.authorityService.createAuthority(AuthorityType.GROUP, groupName);
		// Création d'un utilisateur à placer dans le groupe
		Map<QName, Serializable> properties = new HashMap<QName, Serializable>(1);
		properties.put(ContentModel.PROP_USERNAME, userOfGroup);
		this.personService.createPerson(properties);
		this.authorityService.addAuthority(PermissionService.GROUP_PREFIX + groupName, userOfGroup);
		
		// Création d'un utilisateur non membre du groupe
		properties.put(ContentModel.PROP_USERNAME, userOutOfGroup);
		this.personService.createPerson(properties);
	}

	@After
	public void TearDown() {
		AuthenticationUtil.setFullyAuthenticatedUser(AuthenticationUtil.getAdminUserName());
		// Suppression de nos utilisateurs et groupe de test
		this.authorityService.deleteAuthority(PermissionService.GROUP_PREFIX + groupName);
		this.personService.deletePerson(userOfGroup);
		this.personService.deletePerson(userOutOfGroup);
	}

	/**
	 * Test à effectuer quek que soit le cas de test utilisateur
	 */
	private NodeRef testTousUtilisateurs() {
		AuthenticationUtil.setFullyAuthenticatedUser(AuthenticationUtil.getAdminUserName());

		// Récupération du dossier où classer le document
		NodeRef companyHome = nodeLocatorService.getNode("companyhome", null, null);
		assertNotNull(companyHome);

		// création du document
		String documentName = "MonDocument" + UUID.randomUUID();
		FileInfo documentInfo = this.fileFolderService.create(companyHome, documentName, ContentModel.TYPE_CONTENT);
		assertNotNull(documentInfo);

		// récupération du node ref
		NodeRef documentNodeRef = documentInfo.getNodeRef();

		// Vérification du nom (au cas où un behaviour serait exécuté)
		String newDocumentName = (String) this.nodeService.getProperty(documentNodeRef, ContentModel.PROP_NAME);
		assertNotNull(newDocumentName);

		// Vérification que les 2 utilisateurs peuvent accéder au document
		AuthenticationUtil.setFullyAuthenticatedUser(userOfGroup);
		this.nodeService.getProperty(documentNodeRef, ContentModel.PROP_NAME);
		AuthenticationUtil.setFullyAuthenticatedUser(userOutOfGroup);
		this.nodeService.getProperty(documentNodeRef, ContentModel.PROP_NAME);

		// Exécution de l'action
		AuthenticationUtil.setFullyAuthenticatedUser(AuthenticationUtil.getAdminUserName());
		Map<String, Serializable> params = new HashMap<String, Serializable>(1);
		params.put(SetStatusAction.PARAM_STATUS, "Validé");
		params.put(SetStatusAction.PARAM_GROUP, PermissionService.GROUP_PREFIX + "TestGroup");
		Action action = this.actionService.createAction(SetStatusAction.NAME, params);
		this.actionService.executeAction(action, documentNodeRef);

		// Récupération du nouveau status
		String statut = (String) this.nodeService.getProperty(documentNodeRef, DocubaseModel.PROP_STATUT2);
		assertNotNull(statut);
		assertEquals("Validé", statut);
		
		return documentNodeRef;
	}
	
	@Test
	public void testActionStatusUserOfGroup() {
		NodeRef documentNodeRef = testTousUtilisateurs();
		
		// Vérification que l'utilisateur du groupe à toujours accès au document
		AuthenticationUtil.setFullyAuthenticatedUser(userOfGroup);
		this.nodeService.getProperty(documentNodeRef, ContentModel.PROP_NAME);
	}
	
	@Test(expected = AccessDeniedException.class)
	public void testActionStatusUserOutOfGroup() {
		NodeRef documentNodeRef = testTousUtilisateurs();
		
		// Le test avec l'utilisateur non membre du groupe doit générer une exception
		AuthenticationUtil.setFullyAuthenticatedUser(userOutOfGroup);
		this.nodeService.getProperty(documentNodeRef, ContentModel.PROP_NAME);		
	}

}
