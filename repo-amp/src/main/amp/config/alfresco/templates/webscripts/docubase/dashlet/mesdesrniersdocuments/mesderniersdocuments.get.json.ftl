<#import "/org/alfresco/slingshot/documentlibrary/item.lib.ftl" as itemLib />
<#assign workingCopyLabel = " " + message("coci_service.working_copy_label")>
<#escape x as jsonUtils.encodeJSONString(x)>
{
   "totalRecords": ${results?size},
   "startIndex": 1,
   "metadata":
   {
      "repositoryId": "${server.id}",
      "onlineEditing": false,
      "itemCounts":
      {
         "folders": 0,
         "documents": ${results?size}
      },
      "workingCopyLabel": "${workingCopyLabel}"
   },
   "items":
   [
      <#list results as item>
      {
         <@itemLib.itemJSON item=item />
      }<#if item_has_next>,</#if>
      </#list>
   ]
}
</#escape>
