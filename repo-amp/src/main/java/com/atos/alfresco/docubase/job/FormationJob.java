package com.atos.alfresco.docubase.job;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.schedule.AbstractScheduledLockedJob;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class FormationJob extends AbstractScheduledLockedJob {
	private final static String JOB_REF_BEAN_KEY = "process";
	
	@Override
	public void executeJob(JobExecutionContext jobContext) throws JobExecutionException {
		JobDataMap jobData = jobContext.getJobDetail().getJobDataMap();
		Object process = jobData.get(JOB_REF_BEAN_KEY);
		if (process == null) {
			throw new AlfrescoRuntimeException("Le process associé au job n'est pas défini");
		}
		
		FormationJobProcess jobProcess = (FormationJobProcess) process;
		jobProcess.run();
	}

}
