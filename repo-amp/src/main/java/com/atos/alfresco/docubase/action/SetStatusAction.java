package com.atos.alfresco.docubase.action;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.QName;

import com.atos.alfresco.docubase.model.DocubaseModel;

/**
 * Application de notre aspect custom, du statut et limitation des droits à un groupe.
 * 
 * @author cleseach
 *
 */
public class SetStatusAction extends ActionExecuterAbstractBase {
	public final static String NAME = "actionSetStatut";
	public final static String PARAM_STATUS = "status";
	public final static String PARAM_GROUP = "groupe";
	
	private NodeService nodeService;
	private PermissionService permissionService;
	
	@Override
	protected void executeImpl(Action action, NodeRef actionedUponNodeRef) {
		// Définition du statut
		String status = (String) action.getParameterValue(PARAM_STATUS);
		Map<QName, Serializable> properties = new HashMap<QName, Serializable>(1);
		properties.put(DocubaseModel.PROP_STATUT2, status);
		this.nodeService.addAspect(actionedUponNodeRef, DocubaseModel.ASPECT_STATUT, properties);
		
		// Limitation des droits
		// On commence par couper l'héritage des droits pour reprendre le contrôle total sur le noeud
		this.permissionService.setInheritParentPermissions(actionedUponNodeRef, false);
		// Application du droit de contribution au groupe
		String group = (String) action.getParameterValue(PARAM_GROUP);
		this.permissionService.setPermission(actionedUponNodeRef, group, PermissionService.EDITOR, true);
		
	}

	@Override
	protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
		paramList.add(new ParameterDefinitionImpl(PARAM_STATUS, DataTypeDefinition.TEXT, true, getParamDisplayLabel(PARAM_STATUS), false, "ac-status"));		
		paramList.add(new ParameterDefinitionImpl(PARAM_GROUP, DataTypeDefinition.TEXT, true, getParamDisplayLabel(PARAM_GROUP), false, "ac-group"));		
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}
	
	

}
